import requests
from django.http import JsonResponse

def get_photo(request):
    
   response={
    "config": {
        "ballSmoothingWeight": 0.5,
        "circleRadiusToMinDim": 0.45,
        "compressDepthMap": True,
        "debugOutput": False,
        "depthMapBits": 16,
        "distNormalMaxDeviation": 0.2,
        "dontShowReadinessTimeoutMs": 1000,
        "encryptionChain": "TransactionIdSessionIdPinAndCertificates",
        "faceBestHeight": 0.6,
        "firstImgFormat": ".jpg",
        "fitFaceMultNear": 0.75,
        "holdStillNearTimeoutMs": 150,
        "holdStillNormalTimeoutMs": 1500,
        "jpgQualityPercent": 80,
        "maxPitchNear": 10,
        "maxPitchNormal": 12,
        "maxRollNormal": 10,
        "maxYawNear": 12,
        "maxYawNormal": 10,
        "messageQueueSize": 1,
        "minBrightness": -1.5,
        "minHoldStillFraction": 2,
        "minReadinessNear": 0.9,
        "minReadinessNormal": 0.7,
        "minSharpness": 0,
        "moveAwayMultNear": 1.5,
        "moveCloserMultNear": 1.3,
        "nearTimeoutMs": 60000,
        "normalTimeoutMs": 60000,
        "numSectors": 8,
        "pin": [
            5,
            0,
            3,
            6
        ],
        "pngCompression": 3,
        "recalculateLandmarks": True,
        "resizedFrameHeight": 512,
        "resizedFrameWidth": 512,
        "rotationAngle": 0,
        "scenario": 0,
        "showGraySectors": False,
        "showHintDurationMs": 1200,
        "showHintTimeoutMs": 1500,
        "sizeNormalMaxDeviation": 0.2,
        "targetSectorRelChange": 1
    },
    "publicKey": "{\"buffer\":\"BF/tGLmB/saawvTAgLagiQL/iPbHC0EBAl3jaE5ufBzbaga8DdRW9ovAeelNnjrx8hEbEGhuRJZlgWlV88cJmKo=\",\"curveType\":\"prime256v1\",\"keyType\":\"publicKey\"}",
    "schema": "default",
    "sessionId": "3697c67f-8bf2-4328-ad46-15202646526d",
    "tag": "3697c67f-8bf2-4328-ad46-15202646526d",
    "transactionId": "0e66e028-e7e9-4fb4-805e-018c5a79ead7",
    "type": 0,
    "metadata": {
        "serverTime": "2024-06-28T12:45:42.467698",
        "ctx": {
            "userIp": "92.63.207.64"
        }
    }
}
   return response