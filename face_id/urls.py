from django.urls import path
from .views import get_photo

urlpatterns = [
    path("v2/liveness/start/",get_photo),
]